Refinery::Core::Engine.routes.draw do

  # Admin routes
  namespace :site_settings, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :site_settings, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
