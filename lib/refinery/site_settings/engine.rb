module Refinery
  module SiteSettings
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::SiteSettings

      engine_name :refinery_site_settings

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "site_settings"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.site_settings_admin_site_settings_path }
          plugin.pathname = root
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::SiteSettings)
      end
    end
  end
end
