module Refinery
  module SiteSettings
    module Admin
      class SiteSettingsController < ::Refinery::AdminController

        crudify :'refinery/site_settings/site_setting',
                :title_attribute => 'name'

        def edit
          @site_setting = SiteSetting.find(params[:id])
        end

        def new
          @site_setting = SiteSetting.new
        end

        private

        # Only allow a trusted parameter "white list" through.
        def site_setting_params
          params.require(:site_setting).permit(:name, :type, :value, :comment, :deletable)
        end
      end
    end
  end
end
