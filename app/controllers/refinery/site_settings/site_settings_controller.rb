module Refinery
  module SiteSettings
    class SiteSettingsController < ::ApplicationController

      before_action :find_all_site_settings
      before_action :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @site_setting in the line below:
        present(@page)
      end

      def show
        @site_setting = SiteSetting.find(params[:id])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @site_setting in the line below:
        present(@page)
      end

    protected

      def find_all_site_settings
        @site_settings = SiteSetting.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/site_settings").first
      end

    end
  end
end
