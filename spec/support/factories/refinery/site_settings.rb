
FactoryGirl.define do
  factory :site_setting, :class => Refinery::SiteSettings::SiteSetting do
    sequence(:name) { |n| "refinery#{n}" }
  end
end

