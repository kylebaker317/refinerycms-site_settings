class AddCommentToRefinerySiteSettings < ActiveRecord::Migration
  def change
    add_column :refinery_site_settings, :comment, :text
    add_column :refinery_site_settings, :deletable, :boolean
  end
end
