class CreateSiteSettingsSiteSettings < ActiveRecord::Migration

  def up
    create_table :refinery_site_settings do |t|
      t.string :name
      t.string :type
      t.string :value
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-site_settings"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/site_settings/site_settings"})
    end

    drop_table :refinery_site_settings

  end

end
