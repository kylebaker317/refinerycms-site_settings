Refinery::I18n.frontend_locales.each do |lang|
  I18n.locale = lang

  Refinery::User.find_each do |user|
    user.plugins.where(name: 'refinerycms-site_settings').first_or_create!(
        position: (user.plugins.maximum(:position) || -1) +1
    )
  end if defined?(Refinery::User)

  def default_settings
    Refinery::SiteSettings::SiteSetting.create name: 'menu_style', value: 'style_1'
  end
  default_settings
end
