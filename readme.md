# Site Settings extension for Refinery CMS.

## How to build this extension as a gem (not required)

    cd vendor/extensions/site_settings
    gem build refinerycms-site_settings.gemspec
    gem install refinerycms-site_settings.gem

    # Sign up for a https://rubygems.org/ account and publish the gem
    gem push refinerycms-site_settings.gem
